import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { Ionicons } from '@expo/vector-icons';
import { createDrawerNavigator } from '@react-navigation/drawer';

import {CustomHeaders, CustomDrawerContent} from './components';
import {Accueil, Profile, DetailReglages, Reglages} from './components/tab';
import {Cours, Quizz, Exercices, Telechargements} from './components/drawer';
import {UniteDenseignement, PdfView, Chapitres,
        Modules, ClassesParUE, Supports,
        OptionsParClasses, UeParModules} from './components/option';
import {Mot_de_PO, Inscription, Connections} from './components/auth';
import {AddCours} from './components/baseDonnees';



const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const navOptionHandler = () =>({
  headerShown: false
})

const StackAccueil = createStackNavigator();
function AccueilStack(){
  return(
    <StackAccueil.Navigator initialRouteName="Accueil">
        <StackAccueil.Screen name="Accueil" component={Accueil} options={navOptionHandler}/>
        <StackAccueil.Screen name="Profile" component={Profile} options={navOptionHandler}/>
      </StackAccueil.Navigator>
  );
}

const StackReglages = createStackNavigator();
function ReglagesStack(){
  return(
    <StackReglages.Navigator initialRouteName="Reglages">
        <StackReglages.Screen name="Reglages" component={Reglages} options={navOptionHandler}/>
        <StackReglages.Screen name="DetailReglages" component={DetailReglages} options={navOptionHandler}/>
      </StackReglages.Navigator>
  );
}

const StackOptions = createStackNavigator();
function OptionsStack(){
  return(
    <StackOptions.Navigator initialRouteName="UniteDenseignement">
        <StackOptions.Screen name="UniteDenseignement" component={UniteDenseignement} options={navOptionHandler}/>
        <StackOptions.Screen name="Chapitres" component={Chapitres} options={navOptionHandler}/>
        <StackOptions.Screen name="ClassesParUE" component={ClassesParUE} options={navOptionHandler}/>
        <StackOptions.Screen name="Supports" component={Supports} options={navOptionHandler}/>
        <StackOptions.Screen name="OptionsParClasses" component={OptionsParClasses} options={navOptionHandler}/>
        <StackOptions.Screen name="UeParModules" component={UeParModules} options={navOptionHandler}/>
        <StackOptions.Screen name="Modules" component={Modules} options={navOptionHandler}/>
        <StackOptions.Screen name="PdfView" component={PdfView} options={navOptionHandler}/>
      </StackOptions.Navigator>
  );
}

const StackInscription = createStackNavigator();
function InscriptionStack(){
  return(
    <StackInscription.Navigator initialRouteName="Inscription">
        <StackInscription.Screen name="Inscription" component={Inscription} options={navOptionHandler}/>
      </StackInscription.Navigator>
  );
}

const StackConnections = createStackNavigator();
function ConnectionsStack(){
  return(
    <StackConnections.Navigator initialRouteName="Connections">
        <StackConnections.Screen name="Connections" component={Connections} options={navOptionHandler}/>
      </StackConnections.Navigator>
  );
}

const StackAddCours = createStackNavigator();
function AddCoursStack(){
  return(
    <StackAddCours.Navigator initialRouteName="AddCours">
        <StackAddCours.Screen name="AddCours" component={AddCours} options={navOptionHandler}/>
      </StackAddCours.Navigator>
  );
}


function TabNavigator(){
  return(
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size, style }) => {
          let iconName;

          if (route.name === 'Accueil') {
            iconName=
                  focused
                ? 'ios-home'
                : 'md-home'
            }
            else if (route.name === 'AddCours') {
              iconName=focused
                    ?'ios-add-circle'
                    :'md-add-circle'
                    color="#f2711d"
              size=48
              style={
                borderRadius: 100,
                width:40,
                height:44,
                marginTop: -35,
              }
            }
            else if (route.name === 'Reglages') {
              iconName=focused
                    ?'ios-settings'
                    :'md-settings'
            }
            return <Ionicons name={iconName} size={size} color={color} style={style}/>
          },
        })}
        tabBarOptions={{
          activeTintColor: '#00b5ae',
          inactiveTintColor: '#B8BBC4',
        }}
      >
      <Tab.Screen name="Accueil" component={AccueilStack} />
      <Tab.Screen name="AddCours" component={AddCoursStack} />
      <Tab.Screen name="Reglages" component={ReglagesStack} />
    </Tab.Navigator>
  )
}

const Drawer = createDrawerNavigator();

function DrawerNavigator({navigation}){
  return(
    <Drawer.Navigator initialRouteName="MenuTab" drawerContent={() => <CustomDrawerContent navigation={navigation}/>}>
      <Drawer.Screen name="MenuTab" component={TabNavigator}/>
      <Drawer.Screen name="Cours" component={Cours}/>
      <Drawer.Screen name="UniteDenseignement" component={UniteDenseignement}/>
      <Drawer.Screen name="Chapitres" component={Chapitres}/>
      <Drawer.Screen name="ClassesParUE" component={ClassesParUE}/>
      <Drawer.Screen name="Supports" component={Supports}/>
      <Drawer.Screen name="OptionsParClasses" component={OptionsParClasses}/>
      <Drawer.Screen name="UeParModules" component={UeParModules}/>
      <Drawer.Screen name="Modules" component={Modules}/>
    </Drawer.Navigator>
  )
}

const StackApp = createStackNavigator();

export default function App({navigation}) {
  return (
    <NavigationContainer>
      <StackApp.Navigator initialRouteName="Connections">
        <StackApp.Screen name="HomeApp" component={DrawerNavigator} options={navOptionHandler}/>
        <StackApp.Screen name="Connections" component={Connections} options={navOptionHandler}/>
        <StackApp.Screen name="Mot_de_PO" component={Mot_de_PO} options={navOptionHandler}/>
        <StackApp.Screen name="Inscription" component={Inscription} options={navOptionHandler}/>
        <StackApp.Screen name="AddCours" component={AddCours} options={navOptionHandler}/>
        <StackApp.Screen name="UniteDenseignement" component={UniteDenseignement} options={navOptionHandler}/>
        <StackApp.Screen name="Chapitres" component={Chapitres} options={navOptionHandler}/>
        <StackApp.Screen name="ClassesParUE" component={ClassesParUE} options={navOptionHandler}/>
        <StackApp.Screen name="Supports" component={Supports} options={navOptionHandler}/>
        <StackApp.Screen name="OptionsParClasses" component={OptionsParClasses} options={navOptionHandler}/>
        <StackApp.Screen name="UeParModules" component={UeParModules} options={navOptionHandler}/>
        <StackApp.Screen name="Modules" component={Modules} options={navOptionHandler}/>
        <StackApp.Screen name="Quizz" component={Quizz} options={navOptionHandler}/>
        <StackApp.Screen name="Exercices" component={Exercices} options={navOptionHandler}/>
        <StackApp.Screen name="Telechargements" component={Telechargements} options={navOptionHandler}/>
        <StackApp.Screen name="PdfView" component={PdfView} options={navOptionHandler}/>
      </StackApp.Navigator>
    </NavigationContainer>
  );
}