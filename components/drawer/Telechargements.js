import React, {Component} from 'react';
import {View} from 'react-native';
import {Text} from 'native-base';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';


export class Telechargements extends Component{
  render(){
    return(
        <SafeAreaView style={{ flex: 1 }}>
            <CustomHeaders title="Telechargements" navigation={this.props.navigation}/>
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text> Telechargements Screen!</Text>
            </View>
        </SafeAreaView>
      );
  }
}