import React, {Component} from 'react';
import {Icon, View, Text } from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';




export default class CustomHeaders extends Component{
  render(){
    let {navigation, isHome, title} = this.props
    return(
          <View style={{flexDirection:'row', height:50, backgroundColor:'#31796b'}}>
              <View style={{flex:1, justifyContent:'center',margin:10}}>
                {
                  isHome ?
                  <TouchableOpacity style={{flexDirection:'row', alignItems:'center', marginLeft: 15}} onPress={() => navigation.openDrawer()}>
                    <Icon name="ios-menu" style={{ color:'#ecf0f1'}}
                    resizeMode="contain"/>
                  </TouchableOpacity>
                :
                <TouchableOpacity style={{flexDirection:'row', alignItems:'center'}}
                  onPress={() => navigation.goBack()}>
                  <Icon name="ios-arrow-back" style={{ color:'#ecf0f1'}}/>
                    <Text style={{color:'#ecf0f1', margin:7, fontSize: 16}}>  </Text>
                </TouchableOpacity>

                }
              </View>
              <View style={{flex:1.5, justifyContent: 'center'}}>
                  <Text style={{textAlign: 'center', color:'#ecf0f1',fontSize: 20}}>{title}</Text>
              </View>
              <View style={{flex:1}}></View>
          </View>
    );
  }
}