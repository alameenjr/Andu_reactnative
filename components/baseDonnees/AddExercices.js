import React, { Component } from 'react';
import { View,  TouchableOpacity,  StyleSheet, Text, TextInput } from 'react-native';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';
import * as firebase from 'firebase';
import { Ionicons } from "@expo/vector-icons";
import { ListItem, Left, Icon, Right } from 'native-base';


//const rootRef = firebase.database().ref();
//const coursRef = rootRef.child('cours');

export class AddExercices extends Component {

    state = {
        titre: "",
        categorie: "",
        description: ""
    };
    writeUserData(coursId, titre, categorie) {
      firebase.database().ref().child('exercices').push({
            categorie: this.state.categorie,
            description: this.state.description
        });
      };
    add = () => {
        //this.writeuserdata(this.state.titre, this.state.categorie)
        if(this.state.titre.trim() === '' || this.state.categorie.trim() === ''){
          alert('Veuiilez renseigner tous les champs !');
        }else{
            this.writeUserData(this.state.categorie, this.state.description);
            alert('Enregistrement réussi');
          }

        };

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeaders title="Ajout d'Exercices" navigation={this.props.navigation} />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>


        <View style={styles.form}>
        

        <View style={{marginTop: 15}}>
            <Text style={styles.inputTitle}> Catégorie</Text>
            <TextInput style={styles.input}
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize="none"
                onChangeText={categorie => this.setState({ categorie })}
                value={this.state.categorie}
                returnKeyType="next"
                onSubmitEditing={() => this.description.focus()}
                ref={(input) => this.categorie = input}
                keyboardType="name-phone-pad"
                autoCorrect={false}></TextInput>
        </View>

        <View style={{marginTop: 15}}>
            <Text style={styles.inputTitle}> Description</Text>
            <TextInput style={styles.input}
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize="none"
                onChangeText={description => this.setState({ description })}
                value={this.state.description}
                returnKeyType="next"
                onSubmitEditing={() => this.description.focus()}
                ref={(input) => this.passwordInput = input}
                keyboardType="name-phone-pad"
                autoCorrect={false}></TextInput>
        </View>

        <View style={{marginTop: 15}}>
            <Text style={styles.inputTitle}> Choisir un fichier</Text>
            <View style={{flexDirection: 'row'}}>
                <TextInput style={styles.input}
                    onChangeText={fichier => this.setState({ fichier })}
                    ref={(input) => this.passwordInput = input}
                    value={this.state.fichier}
                    editable={false}
                    autoCapitalize="none"
                />
                  <Icon active name="ios-link" style={{color: '#8A8F9E'}} onPress={() => this.props.navigation.navigate('Cours')}/>
                </View>
        </View>

        </View>
            <TouchableOpacity style={styles.button} onPress={this.add}>
              <Text style={styles.buttontext2}>Ajouter</Text>
            </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }

}


const styles = StyleSheet.create({

  container: {
    justifyContent: "center",
    alignItems: "center"
  },
  inputBox: {
    width: 300,
    height: 50,
    backgroundColor: '#00b5ae',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  buttontext2: {
    fontSize: 16,
    fontWeight: '500',
    color: '#fff',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',

  },
  button: {
    width: 300,
    marginVertical: 10,
    borderRadius: 25,
    backgroundColor: '#f2711d',
    paddingVertical: 13,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50

  },
  form: {
    marginBottom: 48,
    marginHorizontal: 30,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 25
},
inputTitle: {
    color:'#8A8F9E',
    fontSize: 10,
    textTransform: 'uppercase'
},
input: {
    borderBottomColor: '#00b5ae',
    borderBottomWidth: StyleSheet.hairlineWidth,
    width: 300,
    height: 40,
    fontSize: 15,
    color:'#161F3D'
},

});
