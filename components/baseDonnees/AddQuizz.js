import React, { Component } from 'react';
import { View,  TouchableOpacity,  StyleSheet, Text, TextInput } from 'react-native';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';
import Pdf from 'react-native-pdf';



export class AddQuizz extends Component {

    state = {
        titre: "",
    };
    componentDidMount() {
      axios
      .get('https://demo.andu.io/api/supports', tokenConfig())
      //.then((response)=> response.json())
      .then((response) => {
        // If request is good...
        this.setState({
          isLoading: false,
          titre: response.data.data,
        })
        //alert(response.data.data);
      })
      .catch((error) => {
        alert('error ' + error);
      });
    }
  render() {
    if(this.state.titre.trim()==='pdf'){
    const source = require('https://demo.andu.io/api/supports/uploads');
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeaders title="Cours" navigation={this.props.navigation} />
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

        <Pdf
              source={source}
            />;
        
        </View>
      </SafeAreaView>
    );
  }
  }

}


const styles = StyleSheet.create({

  container: {
    justifyContent: "center",
    alignItems: "center"
  },
  inputBox: {
    width: 300,
    height: 50,
    backgroundColor: '#00b5ae',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  buttontext2: {
    fontSize: 16,
    fontWeight: '500',
    color: '#fff',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',

  },
  button: {
    width: 300,
    marginVertical: 10,
    borderRadius: 25,
    backgroundColor: '#f2711d',
    paddingVertical: 13,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50

  },
  form: {
    marginBottom: 48,
    marginHorizontal: 30,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 25
},
inputTitle: {
    color:'#8A8F9E',
    fontSize: 10,
    textTransform: 'uppercase'
},
input: {
    borderBottomColor: '#00b5ae',
    borderBottomWidth: StyleSheet.hairlineWidth,
    width: 300,
    height: 40,
    fontSize: 15,
    color:'#161F3D'
},

});
