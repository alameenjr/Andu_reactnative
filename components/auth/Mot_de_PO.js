import React, { Component } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native';
import * as firebase from 'firebase';

export class Mot_de_PO extends Component {
    state = {
        email: "",
        errorMessage: null
    };

    handlePasswordforget = () => {
        const { email, password } = this.state;

        
    };


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.container1}>
                    <Image style={styles.img} source={require('../images/andu.png')} />
                    <TouchableOpacity style={styles.btnBack} onPress={() => this.props.navigation.navigate('Connections')}>
                        <Image style={styles.img2} source={require('../images/back.png')} />
                    </TouchableOpacity>
                </View>
                <View style={styles.UserInput}>
                    {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                    <TextInput style={styles.inputBox}
                        placeholder="email@adresse.com"
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholderTextColor='#ffffff'
                        autoCapitalize="none"
                        onChangeText={password => this.setState({ password })}
                        value={this.state.password}
                        returnKeyType="next"
                        onSubmitEditing={() => this.passwordInput.focus()}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                </View>

                <View style={styles.connect}>
                    <TouchableOpacity style={styles.button} onPress={this.handlePasswordforget} >
                        <Text style={styles.buttontext2}>Reinitialisé Mot de Passe</Text>
                    </TouchableOpacity>
                    <View style={styles.btnSignUp}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Inscription')}>
                            <Text>Pas encore de compte ?  <Text style={styles.buttontext}>S'inscrire</Text></Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    error: {
        color: '#E9446A',
        fontWeight: "600",
        fontSize: 13,
        textAlign: 'center',
    },

    container: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#ffff'
    },

    UserInput: {
        margin: 0,
        marginTop: 0,
    },

    connect: {
        marginBottom: 150,
    },

    inputBox: {
        width: 300,
        height: 50,
        backgroundColor: '#00b5ae',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },



    buttontext: {
        fontSize: 16,
        fontWeight: '500',
        color: '#f2711d',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttontext2: {
        fontSize: 16,
        fontWeight: '500',
        color: '#fff',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',

    },
    button: {
        width: 300,
        marginVertical: 10,
        borderRadius: 25,
        backgroundColor: '#f2711d',
        paddingVertical: 13,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50

    },

    btnSignUp: {
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },

    img: {
        width: 200,
        height: 100,
        resizeMode: 'center',
        margin: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },

    img2: {
        width: 30,
        height: 30,
        top: 8,
        left:5,
    },

    btnBack: {
        position: "absolute",
        top: 10,
        left:-50,
        borderRadius:40,
        width: 45,
        height: 45,
        marginTop:-50,
        backgroundColor: "#31696b"
    },

    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 100,
        margin: 30
    },
})