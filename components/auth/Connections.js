import React, { Component } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native';
import axios from 'axios';
import { StatusBar } from 'react-native';
import { AsyncStorage } from 'react-native';

//import AsyncStorage from '@react-native-community/async-storage';



export class Connections extends Component {

   state = {
        email: '',
        password: '',
        loading: false,
        errorMessage: null,
   }

    onChangeHandle(state, value){
        this.setState({
            [state]: value,
        })
    }



    LoginTrue() {
        this.setState({
            loading: true
         })

        const { email, password } = this.state;

        if(email && password){
            const req = {
                "email": email,
                "password": password
            }

            axios.post('https://demo.andu.io/api/auth/login/', req)
                .then(response => {
                    console.log(response.data);
                    this.setState({
                        loading: false
                    })

                    AsyncStorage.setItem("token", response.data.token)
                    .then(
                        response => {
                            this.props.navigation.navigate('HomeApp');
                            //alert("Connection avec succes!");
                        }
                    );
                },error => {
                    this.setState({ errorMessage: error.message,loading: false})
                }
                );
            }
            else {
                error => this.setState({ errorMessage: error.message });
                alert("email & password incorrect!");

            }
    }




    render() {
        const { email, password, loading } = this.state;
        return (

            <View style={styles.container}>
                <StatusBar hidden={true}/>
                <View style={styles.container1}>
                    <Image style={styles.img} source={require('../images/andu.png')} />
                </View>
                <View style={styles.UserInput}>

                {this.state.loading &&(
                            <View style={{paddingTop:30}}>
                            {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                            <Text style={styles.load}> Chargement...</Text>
                            </View>
                        )}

                    <TextInput style={styles.inputBox}
                        placeholder="email@adresse.com"
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholderTextColor='#ffffff'
                        autoCapitalize="none"
                        onChangeText={(text) => {
                            this.setState({email: text})
                        }}
                        returnKeyType="next"
                        keyboardType="email-address"
                    />
                    <TextInput style={styles.inputBox}
                        placeholder="Password"
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholderTextColor='#ffffff'
                        onChangeText={(text) => {
                            this.setState({password: text})
                        }}
                        secureTextEntry={true}
                        autoCapitalize="none"
                    />
                    <View style={styles.passForget}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Mot_de_PO')}>
                            <Text>Mot de passe oublié ?</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.connect}>
                    <TouchableOpacity activeOpacity={0.8}
                        style={styles.button} onPress={() =>
                        this.LoginTrue()}
                        disabled={loading}>
                        <Text style={styles.buttontext2}>Se Connecter</Text>
                    </TouchableOpacity>
                    <View style={styles.btnSignUp}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Inscription')}>
                            <Text>Pas encore de compte ?  <Text style={styles.buttontext}>S'inscrire</Text></Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    error: {
        color: '#E9446A',
        fontWeight: "600",
        fontSize: 13,
        textAlign: 'center',
    },

    container: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#ffff'
    },

    UserInput: {
        margin: 0,
        marginTop: 0,
        justifyContent: 'space-around',
        alignItems: 'center',
    },

    connect: {
        marginBottom: 150,
    },

    inputBox: {
        width: 300,
        height: 50,
        backgroundColor: '#00b5ae',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },

    passForget: {
        alignItems: 'center',
        marginLeft: 150,
        margin: 10
    },

    buttontext: {
        fontSize: 16,
        fontWeight: '500',
        color: '#f2711d',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttontext2: {
        fontSize: 16,
        fontWeight: '500',
        color: '#fff',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',

    },
    button: {
        width: 300,
        marginVertical: 10,
        borderRadius: 25,
        backgroundColor: '#f2711d',
        paddingVertical: 13,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50

    },

    btnSignUp: {
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    load: {
        color: '#00b5ae',
    },

    sign: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 16,
        flexDirection: 'row',
        paddingVertical: 16
    },
    img: {
        width: 200,
        height: 100,
        resizeMode: 'center',
        margin: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },

    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 100,
        margin: 30
    },
    textSign: {
        fontSize: 16,
        color: '#000000',
    }
})
