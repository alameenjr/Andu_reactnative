import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, TextInput } from 'react-native';
import { Text } from 'native-base';

export class Inscription extends Component {


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.container1}>
                    <Image style={styles.img} source={require('../images/andu.png')} />
                    <TouchableOpacity style={styles.btnBack} onPress={() => this.props.navigation.navigate('Connections')}>
                        <Image style={styles.img2} source={require('../images/back.png')} />
                    </TouchableOpacity>
                </View>
                <View>
                    
                    <TextInput style={styles.inputBox}
                        placeholder="Prénom et Nom"
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholderTextColor='#ffffff'
                        autoCapitalize="none"
                        onChangeText={FullName => this.setState({ FullName })}
                        value={' '}
                        returnKeyType="next"
                        onSubmitEditing={() => this.emailInput.focus()}
                        keyboardType="name-phone-pad"
                        autoCorrect={false}
                    />
                    <TextInput style={styles.inputBox}
                        placeholder="Adresse Email"
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholderTextColor='#ffffff'
                        onChangeText={email => this.setState({ email })}
                        value={' '}
                        returnKeyType="next"
                        onSubmitEditing={() => this.passwordInput.focus()}
                        ref={(input) => this.emailInput = input}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                    <TextInput style={styles.inputBox}
                        placeholder="Mot de passe"
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholderTextColor='#ffffff'
                        onChangeText={password => this.setState({ password })}
                        value={' '}
                        secureTextEntry={true}
                        returnKeyType="next"
                        onSubmitEditing={() => this.passwordConfirmInput.focus()}
                        ref={(input) => this.passwordInput = input}
                        keyboardType="name-phone-pad"
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                    <TextInput style={styles.inputBox}
                        placeholder="Confirmer votre mot de passe"
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholderTextColor='#ffffff'
                        onChangeText={passwordConfirm => this.setState({ passwordConfirm })}
                        value={' '}
                        secureTextEntry={true}
                        returnKeyType="next"
                        ref={(input) => this.passwordConfirmInput = input}
                        keyboardType="name-phone-pad"
                        autoCapitalize="none"
                        autoCorrect={false}
                    />
                </View>


                <View style={styles.connect}>
                    <TouchableOpacity style={styles.button} onPress={()=> {}}>
                        <Text style={styles.buttontext2}>Inscrire</Text>
                    </TouchableOpacity>
                    <View style={styles.btnSignUp}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Connections')}>
                            <Text>Vous avez déjà un compte !</Text>
                            <Text style={styles.buttontext}>Connectez-vous</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    error: {
        color: '#E9446A',
        fontWeight: "600",
        fontSize: 13,
        textAlign: 'center',
    },

    container: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#ffff'
    },

    UserInput: {
        margin: 0,
        marginTop: 0,
    },

    connect: {
        margin: 20,
        marginBottom: 70,
    },

    inputBox: {
        width: 300,
        height: 50,
        backgroundColor: '#00b5ae',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },

    passForget: {
        alignItems: 'center',
        marginLeft: 150,
        margin: 10
    },

    buttontext: {
        fontSize: 16,
        fontWeight: '500',
        color: '#f2711d',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttontext2: {
        fontSize: 16,
        fontWeight: '500',
        color: '#fff',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',

    },
    button: {
        width: 300,
        marginVertical: 10,
        borderRadius: 25,
        backgroundColor: '#f2711d',
        paddingVertical: 13,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50

    },

    btnSignUp: {
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },

    sign: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginLeft: 16,
        flexDirection: 'row',
        paddingVertical: 16
    },
    img: {
        width: 200,
        height: 100,
        resizeMode: 'center',
        margin: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },

    img2: {
        width: 30,
        height: 30,
        top: 8,
        left: 5,
    },

    btnBack: {
        position: "absolute",
        top: 55,
        left: -50,
        borderRadius: 40,
        width: 45,
        height: 45,
        marginTop: -50,
        backgroundColor: "#31696b"
    },

    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50,
        margin: 20
    },
    textSign: {
        fontSize: 16,
        color: '#000000',
    }
})