import React, { Component } from 'react';
import { SafeAreaView, Text, View, FlatList,StyleSheet,H1, Icon, TouchableOpacity, SectionList, ListItem } from 'react-native';
import CustomHeaders from '../CustomHeaders';
import { Ionicons, Feather } from "@expo/vector-icons";
import { AsyncStorage } from 'react-native';
import axios from 'axios';


export class Chapitres extends Component {

  state = {
        data: [],
        loading: false,
        errorMessage: '',
    };


  componentDidMount(){
    this.getChapitres();
  }
 
  //recupere les modules
      async getChapitres(){
        const module_id=this.props.route.params.module_id;
        console.log('module_id', module_id);
        const token=await AsyncStorage.getItem("token");
        console.log('token', token);

        axios
          .get('https://demo.andu.io/api/chapitresByModule/'+module_id+'/',{
             headers: {
              'Authorization': `jwt ${token}`,
              }
          }
          ).then(response => {
                  console.log('chapitre',response.data)
                  console.log('chapitres',response.data.data)
                  this.setState(() => {
                      return {
                        data:response.data.data,
                        loading: true
                      };
                  });
                })
                .catch(error => {
                  console.log("ue error", error);
                  this.setState({errorMessage: error.message});
                  console.log(user)
          });
      }


      alertItemName = (item) => {
        alert(item.name)
     }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeaders title="Chapitre"  navigation={this.props.navigation} />
        {/* // <View style={styles.container}>
        //   <View style={styles.text}>
        //   <Text style={{ fontSize: 25, fontWeight: '800'}}>Tableau de bord</Text>
        //   <Text style={{ fontSize: 13, fontWeight: '800',fontStyle: 'italic'}}>baba@gmail.com</Text>
        //   </View>

        //   <View style={styles.box}>
        //     <View style={styles.box1}>
        //       <View style={styles.box1a}>
        //         <View style={styles.stat}><Text style={{color: '#fff', fontWeight: '700'}} onPress={() => this.props.navigation.navigate('Cours')}>15</Text></View>
        //       <Feather onPress={() => this.props.navigation.navigate('Cours')}
        //         active name="list" size={50}
        //         style={{
        //           color: '#00b5ae',
        //           margin:5
        //           }}
        //         />
        //         <Text>Cours</Text>
        //       </View>
        //       <View style={styles.box1b}>
        //       <View style={styles.stat}><Text style={{color: '#fff', fontWeight: '700'}} onPress={() => this.props.navigation.navigate('Quizz')}>2</Text></View>
        //       <Feather onPress={() => this.props.navigation.navigate('Quizz')}
        //         active name="bar-chart" size={50}
        //         style={{
        //           color: '#00b5ae',
        //           margin:5
        //           }}
        //         />
        //         <Text>Quizz</Text>
        //       </View>
        //     </View>
        //     <View style={styles.box2}>
        //       <View style={styles.box1a}>
        //       <View style={styles.stat}><Text style={{color: '#fff', fontWeight: '700'}} onPress={() => this.props.navigation.navigate('Exercices')}>1</Text></View>
        //       <Feather onPress={() => this.props.navigation.navigate('Exercices')}
        //         active name="activity" size={50}
        //         style={{
        //           color: '#00b5ae',
        //           margin:5
        //           }}
        //         />
        //         <Text>Exercices</Text>
        //       </View>
        //       <View style={styles.box1b}>
        //       <View style={styles.stat}><Text style={{color: '#fff', fontWeight: '700'}} onPress={() => this.props.navigation.navigate('Telechargements')}>3</Text></View>
        //       <Feather onPress={() => this.props.navigation.navigate('Telechargements')}
        //         active name="download" size={50}
        //         style={{
        //           color: '#00b5ae',
        //           margin:5
        //           }}
        //         />
        //         <Text>Téléchargements</Text>
        //       </View>
        //     </View>
        //   </View>
        // </View>*/}
        {this.state.loading &&(
         <View style={styles.container}>
          {/* <FlatList
            data={this.state.data}
            renderItem={({item}) => <Text style={styles.item}>{item.name}</Text>}
          /> */}
          {
               this.state.data.map((item, index) => (
                  <TouchableOpacity
                     key = {item.id}
                     style = {{padding: 10,
                      marginTop: 3,
                      borderColor: '#f2711d',
                      backgroundColor: '#f2711d',
                      borderRadius: 40,
                      alignItems: 'center'}}
                     onPress = {() => this.props.navigation.navigate('Supports',{chapitre_id:item.id})}>
                     <Text style = {styles.text}>
                        {item.name}
                     </Text>
                  </TouchableOpacity>
               ))
            }
        </View>
        )}
        {!this.state.loading &&(
            <View style={{paddingTop:30}}>
              <Text> LOADING </Text>
            </View>
        )}
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({

  // container: {
  //   flex: 1,
  //   alignItems: 'center',
  //   backgroundColor: '#EFECF4',
  // },

  container: {
    padding: 10,
    marginTop: 3,
    alignItems: 'center',
 },
 text: {
    color: '#fff'
 },

  stat: {
    width:30,
    height:30,
    backgroundColor: '#f2711d',
    borderRadius:50,
    position: 'absolute',
    right: -5,
    top: -10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  // text: {
  //   alignItems: 'center',
  //   marginLeft: 0,
  //   marginVertical: 15
  // },

  
});
