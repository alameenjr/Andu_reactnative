import React, {Component} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {Text} from 'native-base';
import CustomHeaders from '../CustomHeaders';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { AsyncStorage } from 'react-native';
import axios from 'axios';


export class OptionsParClasses extends Component{
  state = {
        data: [],
        loading: true,
        errorMessage: '',
        isFormOpen:false
    };
  async componentDidMount(){
        const token=await AsyncStorage.getItem("token");
        console.log('token', token)
        const option=await AsyncStorage.getItem("option");
        console.log('option', option)
       axios
        .get('https://demo.andu.io/api/options/'+option+'/',{
           headers: {
            'Authorization': `jwt ${token}`,
            }
        }
        ).then(response => {
                console.log('options',response.data)
                console.log('options',response.data.data)
                AsyncStorage.setItem('annee', response.data.data.annee);
                AsyncStorage.setItem('schoolname', response.data.data.school_name);
                this.setState(() => {
                    return {
                      data:response.data.data,
                      loading: false
                    };
                });
              })
              .catch(error => {
                console.log("login error", error);
                this.setState({errorMessage: error.message});
                //console.log(user)
        });

  }
  render(){
    return(
      <SafeAreaView style={{ flex: 1 }}>
      <CustomHeaders title="Options Classes" navigation={this.props.navigation} />

        <Text style={{ fontSize: 25,}}>Options Par Classes</Text>

      <ScrollView>
      <View style={styles.flatlist}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) => this.getItem(item)}
          keyExtractor={item => item.id}
        />
      </View>
      </ScrollView>
    </SafeAreaView>
      );
  }
}

const styles = StyleSheet.create({
  flatlist: {
    flex: 1,
    //alignItems: 'center',
    margin: 10,
    //paddingVertical: 50,
    marginVertical: 20,
  },

  container1: {
    flexGrow: 1,
    flexDirection: 'row',
    margin: 5,
    marginHorizontal: 10
  },

  container: {
    flex: 1,
    backgroundColor: '#EFECF4',
    //marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderWidth: 0.3,
    borderColor: '#f2711d',
    borderRadius: 10,
    flexDirection: 'row'
  },
  title: {
    fontSize: 20,
  },
  profile: {
    width: 50,
    height: 50,
    borderRadius: 40,
    borderWidth: 3,
    borderColor: "#fff"
  },

});
