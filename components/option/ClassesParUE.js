import React, { Component } from 'react';
import { View, FlatList, TouchableOpacity, Image, StyleSheet, Text, TextInput, ListView, ListViewComponent } from 'react-native';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';
import axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';

const tokenConfig = () => {
  // Get token from state
  const token =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InNhbWJhLmthbmVAYW5kdWkuaW8iLCJvcmlnX2lhdCI6MTU4NzUwNTcyNywidXNlcl9pZCI6NTMxLCJlbWFpbCI6InNhbWJhLmthbmVAYW5kdWkuaW8iLCJleHAiOjE1OTUyODE3Mjd9.pVXZA-kT4REQU6_42hex6T2tegn35MtJ0lmQUDSHnAY';

  // Headers
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  // If token, add to headers config
  if (token) {
    config.headers['Authorization'] = `Token ${token}`;
  }
  return config;
};

function ImageChoice() {
  return <Image source={require("../images/alameen.png")} style={styles.profile} />
}

export class ClassesParUE extends Component {
  state = {
    module: [],
    chapitre: [],
    id: "",
    isLoading: true
  };

  moduleItem(item) {
    return (
      <View style={styles.item}>
        <ImageChoice />
        <Text style={styles.title}>{item.chapitre.module.unite.name}</Text>
      </View>
    );
  }

  Read = (item) =>{
    return (
      alert(item.id)
    );
  }


  ChapitreItem(item) {

      return (
        <View>
          <TouchableOpacity
          style={styles.item}
          onPress={this.setState.id = item.id, this.Read}
          onSelect={item.id}
            //onSelect={Read}
          >
          <ImageChoice />
          <Text style={styles.title}>{item.name}</Text>
          </TouchableOpacity>
        </View>
      );
  }


  componentDidMount() {
    axios
    .get('https://demo.andu.io/api/supports', tokenConfig())
    //.then((response)=> response.json())
    .then((response) => {
      // If request is good...
      this.setState({
        isLoading: false,
        module: response.data.data,
        chapitre: response.data.data
      })
      //alert(response.data.data);
    })
    .catch((error) => {
      alert('error ' + error);
    });
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomHeaders title="Classes UE" navigation={this.props.navigation} />

        <Text style={{ fontSize: 25,}}>Classes Par UE</Text>

        <ScrollView>
        <View style={styles.flatlist}>

          <FlatList
            data={this.state.chapitre}
            renderItem={({ item }) => this.moduleItem(item)}
            keyExtractor={item => item.id}
          />
        </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  flatlist: {
    flex: 1,
    //alignItems: 'center',
    margin: 10,
    //paddingVertical: 50,
    marginVertical: 20,
  },

  container1: {
    flexGrow: 1,
    flexDirection: 'row',
    margin: 5,
    marginHorizontal: 10
  },

  container: {
    flex: 1,
    backgroundColor: '#EFECF4',
    //marginTop: Constants.statusBarHeight,
  },
  item: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderWidth: 0.3,
    borderColor: '#f2711d',
    borderRadius: 10,
    flexDirection: 'row'
  },
  title: {
    fontSize: 20,
  },
  profile: {
    width: 50,
    height: 50,
    borderRadius: 40,
    borderWidth: 3,
    borderColor: "#fff"
  },

});
