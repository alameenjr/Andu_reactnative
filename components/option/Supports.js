import React, { Component } from 'react';
import { View, FlatList, TouchableOpacity, Image, StyleSheet, Text, TextInput, ListView, ListViewComponent } from 'react-native';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Ionicons, Feather } from "@expo/vector-icons";
import Constants from 'expo-constants';
import { ScrollView } from 'react-native-gesture-handler';
import { AsyncStorage } from 'react-native';
import axios from 'axios';



export class Supports extends Component {

  state = {
    data: [],
    loading: false,
    errorMessage: '',
};


componentDidMount(){
this.getSupports();
}

//recupere les modules
  async getSupports(){
    const chapitre_id=this.props.route.params.chapitre_id;
    console.log('chapitre_id', chapitre_id);
    const token=await AsyncStorage.getItem("token");
    console.log('token', token);

    axios
      .get('https://demo.andu.io/api/supportsByChapitre/'+chapitre_id+'/',{
         headers: {
          'Authorization': `jwt ${token}`,
          }
      }
      ).then(response => {
              console.log('cours',response.data)
              console.log('courss',response.data.data)
              this.setState(() => {
                  return {
                    data:response.data.data,
                    loading: true
                  };
              });
            })
            .catch(error => {
              console.log("cours error", error);
              this.setState({errorMessage: error.message});
      });
  }

  alertItemName = (item) => {
    alert(item.name)
 }
 

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
            <CustomHeaders title="Support" navigation={this.props.navigation}/>
        <View style={styles.header}>
          <View style={styles.container1}>
              <Text style={{ fontSize: 28, color: '#fff' }}>Catégorie de cours</Text>
          </View>
          <View style={styles.categorieBox}>
            <ScrollView style={styles.cB}>
              <View style={styles.box}></View>
            </ScrollView>
          </View>
        </View>
        {!this.state.loading &&(
          <View style={{paddingTop:10}}>
            <Text> Loading... </Text>
          </View>
      )}

          <Text style={{ fontSize: 30, marginHorizontal: 16, margin: 0, color:'#f2711d' }}>Cours Disponible</Text>
          <ScrollView style={styles.cB}>
        {this.state.loading &&(
         <View style={styles.container}>
          {/* <FlatList
            data={this.state.data}
            renderItem={({item}) => <Text style={styles.item}>{item.name}</Text>}
          /> */}
          {
               this.state.data.map((item, index) => (
                  <TouchableOpacity
                     key = {item.id}
                     style = {{padding: 10,
                      marginTop: 3,
                      borderColor:  '#00b5ae',
                      height: 40,
                      width: 300,
                      borderRadius: 40,
                      backgroundColor: '#f2711d',
                      alignItems: 'center',
                      justifyContent: 'center'}}
                     onPress = {() => this.alertItemName(item)}>
                     <Text style = {styles.text}>
                        {item.name}
                     </Text>
                  </TouchableOpacity>
               ))
            }
        </View>
        )}
        </ScrollView>
    </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  flatlist: {
    flex: 1,
    //alignItems: 'center',
    margin: 10,
    paddingVertical: 50,
    marginVertical: 10,
  },
  box: {
    width: 150,
    height: 150,
    borderRadius: 5,
    backgroundColor: '#fff',
    margin: 5
  },
  categorieBox: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
  },
  cB: {
    flex: 1,
  },
  categorie: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginHorizontal: 10,
  },

  CustomHeaders: {
    alignItems: 'center',
    position: "absolute",
    marginVertical: 10,
    marginHorizontal: 150,
  },

  img2: {
    width: 25,
    height: 25,
  },
  text : {
    color: '#fff'
  },

  btnBack: {
    position: "absolute",
    top: 5,
    borderRadius: 40,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: "#31696b"
  },

  container1: {
    flexDirection: 'row',
    margin: 10,
    marginHorizontal: 5
  },

  header: {
    height: 200,
    backgroundColor: '#00b5ae',
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
    marginHorizontal: 3
  },
  container: {
    flex: 1,
    //marginTop: Constants.statusBarHeight,
    alignItems: 'center',

  },
  item: {
    backgroundColor: '#fff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderWidth: 0.3,
    borderColor: '#f2711d',
    borderRadius: 10,
    flexDirection: 'row'
  },
  title: {
    fontSize: 20,
  },
  profile: {
    width: 50,
    height: 50,
    borderRadius: 40,
    borderWidth: 3,
    borderColor: "#fff"
  },

});
