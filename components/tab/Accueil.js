import React, { Component } from 'react';
import { SafeAreaView, Text, View, FlatList,StyleSheet,H1, Icon, TouchableOpacity, SectionList, ListItem } from 'react-native';
import CustomHeaders from '../CustomHeaders';
import { Ionicons, Feather } from "@expo/vector-icons";
import { AsyncStorage } from 'react-native';
import axios from 'axios';


export class Accueil extends Component {

  // async componentDidMount(){
  //   const token=await AsyncStorage.getItem("token");
  //   if(token){
  state = {
        data: [],
        loading: false,
        errorMessage: '',
    };


  componentDidMount(){
    this.getUserDetails()
    setTimeout(() => {
        this.getOptions();
    }, 3000);

    setTimeout(() => {
        this.getUe();
    }, 5000);
  }
  //RECUPERE les Options
      async getOptions(){
        const token=await AsyncStorage.getItem("token");
        console.log('token', token)
        const option=await AsyncStorage.getItem("option");
            console.log('option', option)
            axios
            .get('https://demo.andu.io/api/options/'+option+'/',{
               headers: {
                'Authorization': `jwt ${token}`,
                }
            }
            ).then(response => {
                    console.log('options',response.data)
                    console.log('options',response.data.data)
                    AsyncStorage.setItem('annee', response.data.data.annee)
                    AsyncStorage.setItem('schoolname', response.data.data.school_name)
                    // this.setState(() => {
                    //     return {
                    //       data:response.data.data,
                    //       loading: true
                    //     };
                    // });
                  })
                  .catch(error => {
                    console.log("options error", error);
                    this.setState({errorMessage: error.message});
                    //console.log(user)
            });
      }
  //recupere les ue
      async getUe(){
        const token=await AsyncStorage.getItem("token");
        console.log('token', token)
        const annee=await AsyncStorage.getItem("annee");
        console.log('annee', annee)
        const schoolname=await AsyncStorage.getItem("schoolname");
        console.log('schoolname', schoolname)
        axios
          .get('https://demo.andu.io/api/unitesByAnnee/'+annee+'/'+ schoolname+'/',{
             headers: {
              'Authorization': `jwt ${token}`,
              }
          }
          ).then(response => {
                  console.log('ues',response.data)
                  console.log('uses',response.data.data)
                  //AsyncStorage.setItem('id_unite', response.data.data.id)
                  this.setState(() => {
                      return {
                        data:response.data.data,
                        loading: true
                      };
                  });
                })
                .catch(error => {
                  console.log("ue error", error);
                  this.setState({errorMessage: error.message});
                  console.log(user)
          });
      }
  //recupere les user details
      async getUserDetails(){
            const token=await AsyncStorage.getItem("token");
            console.log('token', token)
           axios
            .get('https://demo.andu.io/api/me/',{
               headers: {
                'Authorization': `jwt ${token}`,
                }
            }
            ).then(response => {
                    console.log('me',response.data.data);
                    AsyncStorage.setItem("option", response.data.data.option.toString());
                  })
                  .catch(error => {
                    console.log("details user error", error);
                    //console.log(user)
            });
      }

      alertItemName = (item) => {
        alert(item.name)
     }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
          <CustomHeaders title="U. E." isHome={true} navigation={this.props.navigation} />
          {!this.state.loading &&(
              <View style={{paddingTop:30}}>
                <Text> Loading... </Text>
              </View>
          )}
        {/* // <View style={styles.container}>
        //   <View style={styles.text}>
        //   <Text style={{ fontSize: 25, fontWeight: '800'}}>Tableau de bord</Text>
        //   <Text style={{ fontSize: 13, fontWeight: '800',fontStyle: 'italic'}}>baba@gmail.com</Text>
        //   </View>

        //   <View style={styles.box}>
        //     <View style={styles.box1}>
        //       <View style={styles.box1a}>
        //         <View style={styles.stat}><Text style={{color: '#fff', fontWeight: '700'}} onPress={() => this.props.navigation.navigate('Cours')}>15</Text></View>
        //       <Feather onPress={() => this.props.navigation.navigate('Cours')}
        //         active name="list" size={50}
        //         style={{
        //           color: '#00b5ae',
        //           margin:5
        //           }}
        //         />
        //         <Text>Cours</Text>
        //       </View>
        //       <View style={styles.box1b}>
        //       <View style={styles.stat}><Text style={{color: '#fff', fontWeight: '700'}} onPress={() => this.props.navigation.navigate('Quizz')}>2</Text></View>
        //       <Feather onPress={() => this.props.navigation.navigate('Quizz')}
        //         active name="bar-chart" size={50}
        //         style={{
        //           color: '#00b5ae',
        //           margin:5
        //           }}
        //         />
        //         <Text>Quizz</Text>
        //       </View>
        //     </View>
        //     <View style={styles.box2}>
        //       <View style={styles.box1a}>
        //       <View style={styles.stat}><Text style={{color: '#fff', fontWeight: '700'}} onPress={() => this.props.navigation.navigate('Exercices')}>1</Text></View>
        //       <Feather onPress={() => this.props.navigation.navigate('Exercices')}
        //         active name="activity" size={50}
        //         style={{
        //           color: '#00b5ae',
        //           margin:5
        //           }}
        //         />
        //         <Text>Exercices</Text>
        //       </View>
        //       <View style={styles.box1b}>
        //       <View style={styles.stat}><Text style={{color: '#fff', fontWeight: '700'}} onPress={() => this.props.navigation.navigate('Telechargements')}>3</Text></View>
        //       <Feather onPress={() => this.props.navigation.navigate('Telechargements')}
        //         active name="download" size={50}
        //         style={{
        //           color: '#00b5ae',
        //           margin:5
        //           }}
        //         />
        //         <Text>Téléchargements</Text>
        //       </View>
        //     </View>
        //   </View>
        // </View>*/}
        {this.state.loading &&(
         <View>
          {/* <FlatList
            data={this.state.data}
            renderItem={({item}) => <Text style={styles.item}>{item.name}</Text>}
          /> */}
          {
               this.state.data.map((item, index) => (
                  <TouchableOpacity
                     key = {item.id}
                     style = {{padding: 10,
                      marginTop: 3,
                      borderColor: '#f2711d',
                      borderRadius: 40,
                      backgroundColor: '#00b5ae',
                      alignItems: 'center'}}
                     onPress = {() => this.props.navigation.navigate('Modules',{unite_id:item.id})}>
                     <Text style = {{color: '#4f603c'}}>
                        {item.name}
                     </Text>
                  </TouchableOpacity>
               ))
            }

        </View>
        )}

      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({

  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#EFECF4',
  },


  stat: {
    width:30,
    height:30,
    backgroundColor: '#f2711d',
    borderRadius:50,
    position: 'absolute',
    right: -5,
    top: -10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  text: {
    alignItems: 'center',
    marginLeft: 0,
    marginVertical: 15
  },

  box: {
    alignContent: 'space-around',
    justifyContent: 'center',
    marginVertical: 0,
  },

  box1: {
    margin: 0,
    padding:10,
    flexDirection: 'row',
    alignContent: 'space-around',
  },
  box2: {
    margin: 0,
    padding:10,
    flexDirection: 'row',
    alignContent: 'space-around',
    marginVertical: -15,
  },

  box1a: {
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    margin:10,
    backgroundColor: '#ffff',
    borderWidth:0.5,
    borderColor: '#f2711d',
    borderRadius: 5
  },

  box1b: {
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    margin:10,
    backgroundColor: '#ffff',
    borderWidth:0.5,
    borderColor: '#f2711d',
    borderRadius: 5
  }


});
