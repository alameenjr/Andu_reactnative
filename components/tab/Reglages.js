import React, {Component} from 'react';
import {View} from 'react-native';
import {Text} from 'native-base';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity } from 'react-native-gesture-handler';


export class Reglages extends Component{
  render(){
  return (
    <SafeAreaView style={{ flex: 1}}>
      <CustomHeaders title="Reglages" isHome={true} navigation={this.props.navigation}/>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Reglages</Text>
        <TouchableOpacity style={{marginTop:20}}
          onPress={() => this.props.navigation.navigate('DetailReglages')}>
          <Text>Detail Reglages</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
  }
}
