import  React, {Component}  from 'react';
import {Text, View} from 'native-base';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';


export class DetailAccueil extends Component {
  render(){
    return (
        <SafeAreaView style={{ flex: 1 }}>
          <CustomHeaders title="DetailAccueil" navigation={this.props.navigation}/>
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Accueil detail!</Text>
          </View>
        </SafeAreaView>
      );
   }
}