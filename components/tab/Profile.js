import React, { Component } from 'react';
import { ImageBackground, Image, StyleSheet, Text, View, TextInput, TouchableOpacity } from "react-native";
import { ScrollView } from 'react-native-gesture-handler';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Ionicons } from "@expo/vector-icons";
import { AsyncStorage } from 'react-native';
import axios from 'axios';




export class Profile extends Component {

    state = {
        data: {},
        loading: true,
        errorMessage: '',
    };

    async componentDidMount(){
        const token=await AsyncStorage.getItem("token");
        console.log('token', token)
       axios
        .get('https://demo.andu.io/api/me/',{
           headers: {
            'Authorization': `jwt ${token}`,
            }
        }
        ).then(response => {
                console.log('me',response.data)
                console.log('me',response.data.data)
                this.setState(() => {
                    return {
                      data:response.data.data,
                      loading: false
                    };
                });
              })
              .catch(error => {
                console.log("login error", error);
                this.setState({errorMessage: error.message});
                //console.log(user)
        });

    }

    render(){
        return (
            <SafeAreaView style={styles.container}>
                <CustomHeaders title="Profile" navigation={this.props.navigation}/>
                <ScrollView>
                    <ImageBackground
                        source={require("../images/alameen.png")}
                        style={{width: undefined, padding:16, paddingTop: 48}}>
                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <Image source={require("../images/al.png")} style={styles.profile}/>
                        </View>

                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={styles.name}>{this.state.data.first_name} {this.state.data.last_name}</Text>
                        </View>

                        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={styles.mail}>{this.state.data.email}</Text>
                            <Ionicons name="ios-mail" size={16} color="rgba(255, 255, 255, 0.8)"/>
                        </View>
                    </ImageBackground>

                    <View style={styles.form}>
                    <View>
                        <Text style={styles.inputTitle}> Nom </Text>
                        <TextInput style={styles.input}
                            value={this.state.data.last_name}
                            editable={false}
                            autoCapitalize="none"></TextInput>
                    </View>

                    <View style={{marginTop: 15}}>
                        <Text style={styles.inputTitle}> Prénom</Text>
                        <TextInput style={styles.input}
                            value={this.state.data.first_name}
                            editable={false}
                            autoCapitalize="none"></TextInput>
                    </View>


                    <View style={{marginTop: 15}}>
                        <Text style={styles.inputTitle}> Adresse e-mail</Text>
                        <TextInput style={styles.input}
                            value={this.state.data.email}
                            editable={false}
                            autoCapitalize="none"></TextInput>
                    </View>

                    <View style={{marginTop: 15}}>
                        <Text style={styles.inputTitle}> School</Text>
                        <TextInput style={styles.input}
                            value={this.state.data.school_name}
                            editable={false}
                            autoCapitalize="none"></TextInput>
                    </View>

                    <View style={{marginTop: 15}}>
                        <Text style={styles.inputTitle}> Profession</Text>
                        <TextInput style={styles.input}
                            value={this.state.data.user_type}
                            editable={false}
                            autoCapitalize="none"></TextInput>
                    </View>

                    <View style={{marginTop: 15}}>
                        <Text style={styles.inputTitle}> Téléphone</Text>
                        <TextInput style={styles.input}
                            value={this.state.data.phone}
                            editable={false}
                            autoCapitalize="none"></TextInput>
                    </View>

                    <TouchableOpacity style={styles.button} >
                        <Text style={{color:"#FFF", fontWeight:"500"}}>Mettre à jour votre profile</Text>
                    </TouchableOpacity>

                </View>

                </ScrollView>
            </SafeAreaView>
        );
        }
      }

const styles = StyleSheet.create({

    container: {
        flex: 1
        },
        profile:{
            width: 80,
            height: 80,
            flexDirection: 'row',
            borderRadius: 40,
            borderWidth: 3,
            borderColor: "#fff"
        },
        name: {
            color: '#fff',
            fontSize: 20,
            fontWeight: "800",
            marginVertical: 8
        },
        mail: {
            color: "#000",
            fontSize: 13,
            marginRight: 4,
        },
        form: {
            marginBottom: 48,
            marginHorizontal: 30,
            alignItems: 'center',
            justifyContent: 'center',
            margin: 25
        },
        inputTitle: {
            color:'#8A8F9E',
            fontSize: 10,
            textTransform: 'uppercase'
        },
        input: {
            borderBottomColor: '#8A8F9E',
            borderBottomWidth: StyleSheet.hairlineWidth,
            width: 300,
            height: 40,
            fontSize: 15,
            color:'#161F3D'
        },
        button: {
            width: 300,
            marginVertical: 40,
            borderRadius: 25,
            backgroundColor: '#00b5ae',
            paddingVertical: 15,
            alignItems: 'center',
            justifyContent: 'center'
        }
})
