import React, {Component} from 'react';
import {View} from 'react-native';
import {Text} from 'native-base';
import CustomHeaders from '../CustomHeaders';
import { SafeAreaView } from 'react-native-safe-area-context';


export class DetailReglages extends Component{
  render(){
    return (
        <SafeAreaView style={{ flex: 1 }}>
          <CustomHeaders title="DetailReglages" navigation={this.props.navigation}/>
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text>Detail Reglages</Text>
          </View>
        </SafeAreaView>
      );
  }
}