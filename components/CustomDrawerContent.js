import React, { Component } from "react";
import { ScrollView} from 'react-native-gesture-handler';
import { Container, Header, Content, ListItem, Icon, Body, Right, Left, Button} from 'native-base';
import { ImageBackground, Image, StyleSheet, Text, View } from "react-native";
import { Ionicons, Feather } from "@expo/vector-icons";
import { AsyncStorage } from 'react-native';
import axios from 'axios';



export class CustomDrawerContent extends Component {
    state = {
        data: {},
        loading: true,
        errorMessage: '',
    };

    async componentDidMount(){
        const token=await AsyncStorage.getItem("token");
        console.log('token', token)
       axios
        .get('https://demo.andu.io/api/me/',{
           headers: {
            'Authorization': `jwt ${token}`,
            }
        }
        ).then(response => {
                console.log('me',response.data)
                console.log('me',response.data.data)
                this.setState(() => {
                    return {
                      data:response.data.data,
                      loading: false
                    };
                });
              })
              .catch(error => {
                console.log("login error", error);
                this.setState({errorMessage: error.message});
                //console.log(user)
        });

    }

    async logout() {
        AsyncStorage.removeItem("token");
       this.props.navigation.navigate('Connections');
    }



    render() {
        return(
            <ScrollView style={styles.container}>
                <ImageBackground
                    source={require("./images/alameen.png")}
                    style={{width: undefined, padding:16, paddingTop: 48}}>
                    <Image source={require("./images/al.png")} style={styles.profile} onPress={() => this.props.navigation.navigate('Profile')}/>
                    <Text style={styles.name}>{this.state.data.first_name} {this.state.data.last_name}</Text>

                    <View style={{flexDirection: 'row'}}>
                        <Text style={styles.statut}>{this.state.data.user_type}</Text>
                        <Ionicons name="md-podium" size={16} color="rgba(255, 255, 255, 0.8)"/>
                    </View>
                </ImageBackground>

                <Container>
                        <Content>
                            <ListItem icon onPress={() => this.props.navigation.navigate('Profile')}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="user" size={16}
                                            style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Profile</Text>
                                </Body>
                            </ListItem>

                            <Header style={{height:0}}/>

                            <ListItem icon onPress={() => this.props.navigation.navigate('UniteDenseignement')}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="bar-chart" size={16}
                                        style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>U. E.</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" style={{color: '#8A8F9E'}}/>
                                </Right>
                            </ListItem>

                            <ListItem icon onPress={() => this.props.navigation.navigate('Modules')}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="list" size={16}
                                        style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Modules</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" style={{color: '#8A8F9E'}}/>
                                </Right>
                            </ListItem>

                            <ListItem icon onPress={() => this.props.navigation.navigate('Chapitres')}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="corner-up-right" size={16}
                                        style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Chapitres</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" style={{color: '#8A8F9E'}}/>
                                </Right>
                            </ListItem>

                            <ListItem icon onPress={() => this.props.navigation.navigate('Supports')}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="activity" size={16}
                                        style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Supports</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" style={{color: '#8A8F9E'}}/>
                                </Right>
                            </ListItem>

                            <ListItem icon onPress={() => this.props.navigation.navigate('Cours')}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="book" size={16}
                                        style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Cours</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" style={{color: '#8A8F9E'}}/>
                                </Right>
                            </ListItem>


                            <ListItem icon onPress={() => this.props.navigation.navigate('ClassesParUE')}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="repeat" size={16}
                                        style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Classes Par UE</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" style={{color: '#8A8F9E'}}/>
                                </Right>
                            </ListItem>

                            <ListItem icon onPress={() => this.props.navigation.navigate('UeParModules')}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="corner-up-left" size={16}
                                        style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>Ue Par Modules</Text>
                                </Body>
                                <Right>
                                    <Icon active name="arrow-forward" style={{color: '#8A8F9E'}}/>
                                </Right>
                            </ListItem>


                            <ListItem icon onPress={() => this.logout()}>
                                <Left>
                                    <Button style={{ backgroundColor: '#fff' }}>
                                        <Feather active name="log-out" size={16}
                                        style={{color: '#8A8F9E',margin:5}} />
                                    </Button>
                                </Left>
                                <Body>
                                    <Text>
                                        Se Déconnecter
                                    </Text>
                                </Body>
                    </ListItem>
                        </Content>
                    </Container>

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1
    },
    profile:{
        width: 80,
        height: 80,
        borderRadius: 40,
        borderWidth: 3,
        borderColor: "#fff"
    },
    name: {
        color: '#fff',
        fontSize: 20,
        fontWeight: "800",
        marginVertical: 8
    },
    statut: {
        color: "rgba(255, 255, 255, 0.8)",
        fontSize: 13,
        marginRight: 4,
    },
    button: {
        width: 280,
        height: 35,
        marginTop:10,
        borderRadius: 25,
        backgroundColor: '#fff',
        paddingVertical: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
});