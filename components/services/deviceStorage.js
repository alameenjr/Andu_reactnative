import {AsyncStorage} from 'react-native';

const deviceStorage = {
    // our AsyncStorage functions will go here :)
    async deleteJWT() {
      try{
        await AsyncStorage.removeItem('id_token')
        .then(
          () => {
            this.setState({
              jwt: ''
            })
          }
        );
      } catch (error) {
        console.log('AsyncStorage Error: ' + error.message);
      }
    }
};

export default deviceStorage;